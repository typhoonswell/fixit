import urllib.request
import shutil
import time

# Download the file from `url` and save it locally under `file_name`:
for i in range(1,85):
    file_name = '{0}.png'.format(i)
    url = 'http://www.boiteajeux.net/jeux/dix/img/{0}'.format(file_name)
    with urllib.request.urlopen(url) as response, open('assets/images/base/{0}'.format(file_name), 'wb') as out_file:
        shutil.copyfileobj(response, out_file)
    print('got ', i)
    time.sleep(1)