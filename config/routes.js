/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': { action: 'lobby' },
  '/:page': { action: 'lobby' },
  'POST /tableEntry': { action: 'table-entry' },
  'GET /tableEntry/:tableId': { action: 'table-entry' },
  'POST /tableLeave': { action: 'table-leave' },
  '/table/:tableId': { action: 'table' },

  '/logout': { action: 'logout' },

  'POST /api/game': { action: 'api/game/create' },
  'GET /api/game/begin': { action: 'api/game/begin' },
  'GET /api/game/state': { action: 'api/game/state' },
  '/api/game/submitClue': { action: 'api/game/submit-clue' },
  '/api/game/submitCardSelection': { action: 'api/game/submit-card-selection' },
  '/api/game/submitCardTableauSelection': { action: 'api/game/submit-card-tableau-selection' },

  'GET /api/table/list': { action: 'api/table/list' },

  'GET /api/chat': { action: 'api/chat/state' },
  'POST /api/chat': { action: 'api/chat/submit' },

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
