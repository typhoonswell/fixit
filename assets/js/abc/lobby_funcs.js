function bumpTableHeartbeat() {
  clearInterval(_TABLELIST_HEARTBEAT);
  tableHeartbeat();
  _TABLELIST_HEARTBEAT = setInterval(tableHeartbeat, 1500);
}

function tableHeartbeat() {
  refreshTable(1,50); //TODO add pagination
}

function setPasswordModal(tableId) {
  if (_USER) {
    $('input[name="tableId"]').val(tableId);
    $('#tablePasswordModal').modal('show');
  }
}

function gotoTable(tableId) {
  if (_USER) {
    window.location.href = '/tableEntry/'+tableId;
  }
}

function refreshTable(mPage, mPagesize) {
  if (typeof refreshTable.selectedTab == 'undefined') {
    refreshTable.selectedTab = 'OPEN';
  }
  $.ajax({
    url: 'api/table/list',
    type: 'get',
    data: {page: mPage, pagesize: mPagesize, tab: refreshTable.selectedTab},
    contentType: 'application/json; charset=utf-8',
    success: function(result) {
      $('#tableListBody').text('');
      $.each(result, function(i, item) {
        var nameField = item.name;
        nameField += (item.password != null ? " <img src='images/feather/lock.svg'/>" : '');
        var $tr = $('<tr>').append(
          $('<td>').html(nameField),
          $('<td>').html(item.owner.username),
          $('<td>').html(item.state),
          $('<td>').html(item.players+'/6')
        );
        if (item.password == null) {
          $tr.click(function(event) {gotoTable(item.id)})
        } else {
          $tr.click(function(event) {setPasswordModal(item.id)})
        }
        $tr.addClass('cursor-pointer');

        $tr.appendTo('#tableListBody');
      });
    }
  });
}

let availableTabs = [ 'ALL', 'OPEN', 'OWNED', 'SEATED', 'CLOSED'];
function setSelectedTab(tab) {
  refreshTable.selectedTab = tab;
  for (let i=0; i < availableTabs.length; i++) {
    if (availableTabs[i] == tab) {
      $('#'+availableTabs[i]+'Tab').addClass('active');
    } else {
      $('#'+availableTabs[i]+'Tab').removeClass('active');
    }
  }
  bumpTableHeartbeat();
}

function submitAndRefresh() {
  $('#createGameSubmitButton').on('click', function(event) {
    $.ajax({
      url: 'api/game',
      method: 'POST',
      data: $('form[name="createGameForm"').serialize(),
      success: function(results) {
        $('#createGameName').val('');
        $('#createGamePassword').val('');
        $('#createGameModal').modal('hide');
        refreshTable(1,20);
      },
    });
  });
}