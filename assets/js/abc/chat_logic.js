function bumpChatHeartbeat() {
  clearInterval(_CHAT_HEARTBEAT);
  chatHeartbeat();
  _CHAT_HEARTBEAT = setInterval(chatHeartbeat, 1500);
}

function chatHeartbeat() {
  if (typeof chatHeartbeat.timestamp == 'undefined') {
    chatHeartbeat.timestamp = 0;
  }

  $.ajax({
    url: '/api/chat',
    method: 'get',
    data: {tableId: _TABLE_ID, timestamp: chatHeartbeat.timestamp},
    success: function(result) {
      if (result.length > 0) {
        chatHeartbeat.timestamp = result[result.length - 1].timestamp;
        populateChat(result);
      }
    }
  });
}

function chatSubmit() {
  $.ajax({
    url: '/api/chat',
    method: 'post',
    data: {
      tableId: _TABLE_ID,
      author: _STATE.player.name,
      text: $('#chatTextInput').val()
    },
    success: function(result) {
      $('#chatTextInput').val('');
      bumpChatHeartbeat();
    }
  });

  $('#chatTextInput').val();
}

function populateChat(entries) {
  for (let i=0; i < entries.length; i++) {
    let chatEntryObject = $('div.chatEntryDiv').clone();
    chatEntryObject.removeClass('chatEntryDiv');
    chatEntryObject.removeClass('invisible');
    chatEntryObject.children('div.chatEntryName').text(entries[i].author+':');
    chatEntryObject.children('div.chatEntryText').text(entries[i].text);
    let chatScrollObject = $('#chatScrollDiv')
    chatScrollObject.append(chatEntryObject);
    chatScrollObject.scrollTop(chatScrollObject.get(0).scrollHeight);
  }
}

function chatInputOverride() {
  $('#chatTextInput').on('keypress', function(event) {
    if (event.which == 13) {
      chatSubmit();
    }
  });
}