function bumpGameHeartbeat() {
  clearInterval(_GAME_HEARTBEAT);
  gameHeartbeat();
  _GAME_HEARTBEAT = setInterval(gameHeartbeat, 1500);
}

function beginGame() {
  $.ajax({
    url: '/api/game/begin',
    type: 'get',
    data: { gameId: _GAME_ID },
    success: function(result) {
      bumpGameHeartbeat();
    }
  });
}

function leaveTable() {
  $.ajax({
    url: '/tableLeave',
    type: 'post',
    data: { tableId: _TABLE_ID },
    success: function(result) {
      window.location.href ='/';
    }
  });
}

function selectFromHandOrTableau(target, cardIndex) {
  if (typeof selectFromHandOrTableau.cardSelectedIndex == 'undefined') {
    selectFromHandOrTableau.cardSelectedIndex = null;
  }
  if (typeof selectFromHandOrTableau.cardLocked == 'undefined') {
    selectFromHandOrTableau.cardLocked = false;
  }

  if (!selectFromHandOrTableau.cardSelectEnabled) {
    return;
  }

  let alterDivPrefix = null;
  let finalizeChoiceAction = null;
  if ( _STATE.currentTurn.phase == 3) {
    if (target == 'HAND') {
      return;
    }
    alterDivPrefix = '#tableauCard';
    finalizeChoiceAction = function() {
      makeTableauNotSelectable(true);
      submitPlayerTableauSelection();
    }
  } else {
    if (target == 'TABLEAU') {
      return;
    }
    alterDivPrefix = '#handDiv';
    finalizeChoiceAction = function() {
      makeHandNotSelectable(true);
      submitPlayerHandSelection();
    }
  }

  // if I'm picking a clue, I want different behavior
  if(_STATE.imTheLead && _STATE.currentTurn.phase == 1) {
    if (cardIndex == selectFromHandOrTableau.cardSelectedIndex) {
      $(alterDivPrefix+cardIndex).addClass('card_hover');
      $(alterDivPrefix+cardIndex).removeClass('card_select');

      selectFromHandOrTableau.cardSelectedIndex = null;
    } else {
      if (selectFromHandOrTableau.cardSelectedIndex != null) {
        $(alterDivPrefix+selectFromHandOrTableau.cardSelectedIndex).addClass('card_hover');
        $(alterDivPrefix+selectFromHandOrTableau.cardSelectedIndex).removeClass('card_select');
      }
      $(alterDivPrefix+cardIndex).removeClass('card_hover');
      $(alterDivPrefix+cardIndex).addClass('card_select');

      selectFromHandOrTableau.cardSelectedIndex = cardIndex;
    }
  } else {
    if (!selectFromHandOrTableau.cardLocked) {
      if (cardIndex == selectFromHandOrTableau.cardSelectedIndex) {
        // submit that card
        selectFromHandOrTableau.cardLocked = true;
        $(alterDivPrefix+cardIndex).addClass('card_select_final');
        finalizeChoiceAction();
      } else {
        // they shouldn't be able to pick their own card for the final phase
        if (_STATE.currentTurn.phase == 3
            && _STATE.tableau[cardIndex-1] == _STATE.tableauPlayerChoices[_STATE.player.name]) {
          return;
        }
        if (selectFromHandOrTableau.cardSelectedIndex != null) {
          $(alterDivPrefix+selectFromHandOrTableau.cardSelectedIndex).addClass('card_hover');
          $(alterDivPrefix+selectFromHandOrTableau.cardSelectedIndex).removeClass('card_select');
        }
        $(alterDivPrefix+cardIndex).removeClass('card_hover');
        $(alterDivPrefix+cardIndex).addClass('card_select');
      }
    }

    selectFromHandOrTableau.cardSelectedIndex = cardIndex;
  }
}

function makeHandNotSelectable(cardLocked) {
  selectFromHandOrTableau.cardSelectEnabled = false;
  for(let i=1; i<=6; i++) {
    $('#handDiv'+i).removeClass('card_hover');
    $('#handDiv'+i).removeClass('card_select');
    if (!cardLocked) {
      $('#handDiv'+i).removeClass('card_select_final');
    }
  }
}

function makeHandSelectable() {
  selectFromHandOrTableau.cardSelectEnabled = true;
  for(let i=1; i<=6; i++) {
    $('#handDiv'+i).addClass('card_hover');
  }
}

function makeTableauNotSelectable(cardLocked) {
  selectFromHandOrTableau.cardSelectEnabled = false;
  for(let i=1; i<=6; i++) {
    $('#tableauCard'+i).removeClass('card_hover');
    $('#tableauCard'+i).removeClass('card_select');
    if (!cardLocked) {
      $('#tableauCard'+i).removeClass('card_select_final');
    }
  }
}

function makeTableauSelectable() {
  selectFromHandOrTableau.cardSelectEnabled = true;
  for(let i=1; i<=6; i++) {
    $('#tableauCard'+i).addClass('card_hover');
  }
}

function showThisAndHideOthers(elem) {
  var elems = [
    'phase1Input',
    'clueTextDiv',
    'phase1WaitText',
    'leaderWaitingDiv',
    'guessLeaderDiv',
    'gameEndedDiv'
  ];
  for(i in elems) {
    if (elems[i] == elem) {
      $('#'+elems[i]).removeClass('d-none');
    } else {
      $('#'+elems[i]).addClass('d-none');
    }
  }
}

function hideTableau() {
  for (let i=1; i<=6; i++) {
    $('#tableauCard'+i).addClass('d-none');
  }
}

function submitClue(event) {
  // can't continue until a card is selected
  if (typeof selectFromHandOrTableau.cardSelectedIndex == 'undefined'
      || selectFromHandOrTableau.cardSelectedIndex == null) {
    return;
  }

  $.ajax({
    url: '/api/game/submitClue',
    method: 'post',
    data: {
      tableId: _TABLE_ID,
      gameId: _STATE.table.servingGame,
      cardIndex: selectFromHandOrTableau.cardSelectedIndex,
      clue: $('#clueText').val()
    },
    success: function(result) {
      $('#clueText').val('');
      bumpGameHeartbeat();
    }
  });
}

function submitClueForm() {
  $('#clueSubmitButton').on('click', submitClue);
  $('#clueText').on('keypress', function(event) {
    if (event.which == 13) {
      submitClue();
    }
  });
}

function initializeMagnifierAction() {
  for (let i=1; i <= 6; i++) {
    $('#handCard'+i).on('contextmenu', function (e) {
      $('#magnifyImg').attr('src', $(this).attr('src'));
      $('#magnifyModal').modal('show');
      return false;
    });
    $('#tableauCard'+i).on('contextmenu', function (e) {
      $('#magnifyImg').attr('src', $(this).attr('src'));
      $('#magnifyModal').modal('show');
      return false;
    });
  }
}

function submitPlayerHandSelection() {
  $.ajax({
    url: '/api/game/submitCardSelection',
    method: 'post',
    data: {
      tableId: _TABLE_ID,
      gameId: _STATE.table.servingGame,
      cardIndex: selectFromHandOrTableau.cardSelectedIndex
    },
    success: function(result) {
      bumpGameHeartbeat();
    }
  });
}

function submitPlayerTableauSelection() {
  $.ajax({
    url: '/api/game/submitCardTableauSelection',
    method: 'post',
    data: {
      tableId: _TABLE_ID,
      gameId: _STATE.table.servingGame,
      cardIndex: selectFromHandOrTableau.cardSelectedIndex
    },
    success: function(result) {
      bumpGameHeartbeat();
    }
  });
}

function gameHeartbeat() {
  var turnNumber = (_STATE == null ? 0 : _STATE.currentTurn.number);
  $.ajax({
    url: '/api/game/state',
    method: 'get',
    data: {gameId: _GAME_ID, turnNumber: turnNumber},
    success: function(result) {
      _STATE = result;
      if (_STATE.table.state == 'CLOSED') {
        // table owner closed the table. nonplayable
        window.location.href='/';
      }
      // update gameboard
      updateGameBoard(_STATE);
      // update scorecard
      updateScorecard(_STATE);
      // update hand
      updateHand(_STATE);
      // update turn history
      updateTurnHistory(_STATE);
    }
  });
}

function updateGameBoard(state) {
  if (typeof updateGameBoard.lastTurnNumber == 'undefined') {
    updateGameBoard.lastTurnNumber == null;
  }
  if (typeof updateGameBoard.lastPhase == 'undefined') {
    updateGameBoard.lastPhase == null;
  }

  if ( state.currentTurn.number != updateGameBoard.lastTurnNumber
       || state.currentTurn.phase != updateGameBoard.lastPhase)
  {
    if (_STATE.currentTurn.number == 0) {
      showThisAndHideOthers('leaderWaitingDiv');
      return;
    }
    selectFromHandOrTableau.cardLocked = false;
    $('#turnsRemainingText').text('Turns Left: ' + _STATE.turnsRemaining);
    switch(state.currentTurn.phase) {
      case 1:
        hideTableau();
        makeTableauNotSelectable(false);
        selectFromHandOrTableau.cardSelectedIndex = null;
        if (_STATE.imTheLead) {
          makeHandSelectable();
          showThisAndHideOthers('phase1Input');
        } else {
          makeHandNotSelectable(false);
          showThisAndHideOthers('phase1WaitText');
        }
        break;
      case 2:
        hideTableau();
        makeTableauNotSelectable(false);
        if (_STATE.imTheLead) {
          makeHandNotSelectable(false);
          showThisAndHideOthers('leaderWaitingDiv');
        } else {
          // if I already selected, highlight the card and turn off selection
          let myHandSelection = _STATE.tableauPlayerChoices[_STATE.player.name];
          if(myHandSelection) {
            makeHandNotSelectable(true);
            let handIndex = _STATE.player.hand.findIndex(val => val == myHandSelection) + 1;
            $('#handDiv'+handIndex).addClass('card_select_final');
          } else {
            makeHandSelectable();
          }
          $('#clueTextH1').text(_STATE.currentTurn.clue);
          showThisAndHideOthers('clueTextDiv');
        }
        break;
      case 3:
        makeHandNotSelectable(false);
        if (_STATE.imTheLead) {
          showThisAndHideOthers('leaderWaitingDiv');
        } else {
          showThisAndHideOthers('guessLeaderDiv');
          $('#clueTextPhase3').text(_STATE.currentTurn.clue);
          makeTableauSelectable();
        }

        let myHandSelection = _STATE.tableauPlayerChoices[_STATE.player.name];
        for (let i=1; i <= _STATE.tableau.length; i++) {
          $('#tableauCard'+i).removeClass('d-none');
          $('#tableauCard'+i).attr('src', '/images/cards/base/'+_STATE.tableau[i-1]+'.png');
          if (myHandSelection == _STATE.tableau[i-1]) {
            $('#tableauCard'+i).addClass('shaded_image');
            $('#tableauCard'+i).removeClass('card_hover');
          } else {
            $('#tableauCard'+i).removeClass('shaded_image');
          }
        }
        for (let i=0; i <= _STATE.finalGuessesSubmitted.length; i++) {
          if (_STATE.finalGuessesSubmitted[i-1] != null) {
            $('#playerScoreCard'+i).addClass('player_select');
          } else {
            $('#playerScoreCard'+i).removeClass('player_select');
          }
        }
        break;
      case 4:
        // last turn ended the game
        hideTableau();
        makeHandNotSelectable(false);
        makeTableauNotSelectable(false);
        showThisAndHideOthers('gameEndedDiv');
        // throw this last turn into the turn history
        _STATE.turnHistory.push(_STATE.currentTurn);
    }

    updateGameBoard.lastTurnNumber = state.currentTurn.number;
    updateGameBoard.lastPhase = state.currentTurn.phase;
  }
}

function updateScorecard(state) {
  // waiting for others to join
  if (state.currentTurn.number == 0) {
    for(let i=1; i <= 6; i++) {
      if (i <= state.playersSeated.length) {
        $('#playerScore'+i).text(0);
        $('#playerName'+i).attr('placeholder', state.playersSeated[i-1]);
        $('#playerScoreCard'+i).removeClass('invisible');
      } else {
        $('#playerScore'+i).text('');
        $('#playerName'+i).attr('placeholder', '<empty seat>');
      }
    }

    if (_STATE.playersSeated[0] == _STATE.player.name) {
      $('#beginGameItem').removeClass('invisible');
      if (state.playersSeated.length >= 4) {
        $('#beginGameButton').removeClass('disabled');
      } else {
        $('#beginGameButton').addClass('disabled');
      }
    }
  } else {
    $('#leaveTableItem').addClass('invisible');
    $('#beginGameItem').addClass('invisible');

    let j=1;
    // put info in correct # of scorecards left to right
    for (const pName in state.playerScores) {
      $('#playerScore'+j).text(state.playerScores[pName]);
      $('#playerName'+j).attr('placeholder', pName);
      j++;
    }
    // finish the above loop to 6 (max players) hiding scorecards
    for (let i = j; i <= 6; i++) {
      $('#playerScoreCard'+i).addClass('d-none');
    }

    for (let i=1; i <= _STATE.currentTurn.playerStates.length; i++) {
      if (_STATE.currentTurn.playerStates[i-1].state == 'WAITING') {
        $('#playerScore'+i).removeClass('player_selecting');
        $('#playerScore'+i).addClass('player_waiting');
      } else {
        $('#playerScore'+i).addClass('player_selecting');
        $('#playerScore'+i).removeClass('player_waiting');
      }
    }
  }
}

function updateHand(state) {
  if (state.currentTurn.number > 0) {
    for(let i=1; i<=6; i++) {
      if (state.player.hand[i-1] != null) {
        $('#handCard'+i).attr('src', '/images/cards/base/'+state.player.hand[i-1]+'.png');
      } else {
        $('#handCard'+i).attr('src', '');
      }
    }
  }
}

function updateTurnHistory(state) {
  if (_STATE.turnHistory.length > 0) {
    $('#turnHistory').removeClass('invisible');
    let crown = '<img src="/images/feather/user.svg" /> ';
    for(let i=0; i<_STATE.turnHistory.length; i++) {
      let newHistoryEntry = $('.historyEntryTemplate').clone();
      newHistoryEntry.children('.turnNumberMarker').text('Turn ' + _STATE.turnHistory[i].number + ' clue: ');
      newHistoryEntry.children('.turnNumberMarker').append('<span class="mx-2"><strong>' + _STATE.turnHistory[i].clue + '</strong></span>');
      newHistoryEntry.removeClass('historyEntryTemplate');

      // populate the snapshot scoreboard
      let playerScoreEntries = newHistoryEntry
        .children('div.contentParentDiv')
        .children('div.scoreTableParentDiv')
        .children('table')
        .children('tbody')
        .children('tr');
      let j = 0;
      for (const sD in _STATE.turnHistory[i].scoreDeltas) {
        let playerTr = $(playerScoreEntries[j]);
        playerTr.children('td.playerNameCell').text(sD);
        playerTr.children('td.playerDeltaCell').text(_STATE.turnHistory[i].scoreDeltas[sD]);
        playerTr.children('td.playerTotalCell').text(_STATE.turnHistory[i].finalState[sD]);
        j++;
      }
      // remove the extra elements
      for (j; j < 6; j++) {
        playerScoreEntries[j].remove();
      }

      // populate the tableau with full details
      let tabCardEntries = newHistoryEntry
        .children('div.contentParentDiv')
        .children('div.tabCardDiv');
      j = 0;
      for (const tC in _STATE.turnHistory[i].cardSelection.tableau) {
        let myCard = $(tabCardEntries.children('div.card')[j]);
        if (_STATE.turnHistory[i].cardSelection.tableau[tC].lead) {
          myCard.children('div.card-header').html(crown);
        }
        myCard.children('div.card-header').append(_STATE.turnHistory[i].cardSelection.tableau[tC].name);
        myCard.children('div.card-body').children('img').attr(
          'src',
          '/images/cards/base/'+_STATE.turnHistory[i].cardSelection.tableau[tC].cardNumber+'.png'
        );
        myCard.children('div.card-footer').text(
          _STATE.turnHistory[i].cardSelection.finalGuesses[_STATE.turnHistory[i].cardSelection.tableau[tC].cardNumber].join(', ')
        );
        j++;
      }
      for (j; j < 6; j++) {
        tabCardEntries[j].remove();
      }
      $('.historyParentUl').prepend(newHistoryEntry);
    }
  }
}