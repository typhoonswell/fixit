/**
 * CardSelection.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    id: { type: 'string', required: true },

    leaderCardNumber: { type: 'number', required: true },

    tableau: { type: 'string', required: true, columnType: 'varchar(1000)' },

    finalGuesses: { type: 'string', required: false, columnType: 'varchar(1000)' }
  },

};

