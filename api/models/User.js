/**
 * User.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    id: { type: 'string', required: true },

    username: { type: 'string', required: true },

    password: { type: 'string', minLength: 1, required: true },

    playerSession: {
        collection: 'player',
        via: 'user'
    },

    ownedTables: { collection: 'tablex', via: 'owner' },
  }

};

