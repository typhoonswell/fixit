/**
 * Game.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    id: { type: 'string', required: true },

    deck: { type: 'string', required: false },

    scores: { type: 'string', required: false, columnType: 'varchar(1000)' },

    endOfGameReason: { type: 'string', required: false, allowNull: true },

    active: { model: 'tablex' },

    turns: { collection: 'turn', via: 'game' }
  },

};