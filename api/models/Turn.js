/**
 * Turn.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id: { type: 'string', required: true },

    number: { type: 'number', required: true },

    clue: { type: 'string', required: false, allowNull: true },

    phase: { type: 'number', required: true },

    playerStates: { type: 'string', required: true, columnType: 'varchar(1000)' },

    leadPlayer: { type: 'number', required: false, defaultsTo: 0 },

    scoreDeltas: { type: 'string', required: false, columnType: 'varchar(1000)' },

    finalState: { type: 'string', required: false, columnType: 'varchar(1000)' },

    game: { model: 'game' },

    cardSelection: { model: 'cardSelection' }
  },

};

