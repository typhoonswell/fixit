/**
 * ChatEntry.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id: { type: 'string', required: true },

    author: { type: 'string', required: true},

    text: { type: 'string', required: true, columnType: 'varchar(1000)' },

    table: { model: 'tablex' },
  },

};

