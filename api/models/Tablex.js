/**
 * Table.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    id: { type: 'string', required: true },

    name: { type: 'string', required: true },

    password: { type: 'string', required: false, allowNull: true },

    state: { type: 'string', required: true, isIn: ['WAITING_FOR_PLAYERS', 'IN_PROGRESS', 'COMPLETE', 'CLOSED']},

    owner: {
        model: 'user'
    },

    players: {
        collection: 'player',
        via: 'seatedAt'
    },

    servingGame: {
        model: 'game'
    },
  },

};

