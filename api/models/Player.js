/**
 * Player.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    id: { type: 'string', required: true },

    name: { type: 'string', required: true },

    hand: { type: 'string', required: false, columnType: 'varchar(1000)' },

    turnGuess: { type: 'number', required: true },

    seatedAt: { model: 'tablex' },

    user: { model: 'user' }
  },

};

