module.exports = {
  friendlyName: 'Dict to string',
  description: '',

  inputs: {
    dict: { type: 'ref', required: true }
  },

  exits: {
    success: {
      description: 'All done.',
    },
  },

  fn: async function (inputs) {
    return '\'' + JSON.stringify(inputs.dict) + '\'';
  }
};

