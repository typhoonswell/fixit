module.exports = {


  friendlyName: 'Passhash',


  description: 'Passhash something.',


  inputs: {
    plaintext: { type: 'string', required: true },
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    var composite = inputs.plaintext;
    return exits.success(
      require('crypto')
        .createHmac('sha256', 'password')
        .update(composite)
        .digest('hex')
    );
  }


};

