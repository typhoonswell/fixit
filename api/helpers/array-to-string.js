module.exports = {
  friendlyName: 'Array to string',
  description: '',

  inputs: {
    array: { type: 'ref', required: true }
  },

  exits: {
    success: {
      description: 'All done.',
    },
  },

  fn: async function (inputs) {
    sails.log.debug('HERE');
    sails.log.debug(JSON.stringify(inputs.array));
    return '\'' + JSON.stringify(inputs.array) + '\'';
  }
};

