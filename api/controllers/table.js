module.exports = {
  friendlyName: 'Table',
  description: 'Table something.',

  inputs: {
    tableId: { type: 'string', required: true }
  },

  exits: {
    success: {
      responseType: 'view',
      viewTemplatePath: 'pages/table'
    }
  },

  fn: async function (inputs, exits) {
    if (this.req.session.chair == null || this.req.session.chair[inputs.tableId] == null) {
      sails.log.info('user tried to enter table without declaring first');
      this.res.redirect('/tableEntry/'+inputs.tableId);
      return;
    }

    let mPlayer = null;
    let table = null;
    let mUser = null;
    await sails.getDatastore().transaction(async (db) => {
      mPlayer = await Player.findOne({id: this.req.session.chair[inputs.tableId]}).usingConnection(db);
      table = await Tablex.findOne({id: inputs.tableId}).usingConnection(db);
      mUser = await User.findOne({id: mPlayer.user}).usingConnection(db);
    });
    delete mUser['password'];

    return exits.success({
      tableId: inputs.tableId,
      tableName: table.name,
      gameId: table.servingGame,
      user: mUser,
      player: mPlayer
    });
  }
};
