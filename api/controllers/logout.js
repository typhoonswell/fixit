module.exports = {
  friendlyName: 'Logout',
  description: 'Logout something.',

  inputs: {
  },

  exits: {
  },

  fn: async function (inputs) {
    if (this.req.session.user != null) {
      this.req.session.user = null;
    }

    if (this.req.session.chair != null) {
      this.req.session.chair = null;
    }

    this.res.redirect('/');
  }
};
