module.exports = {
  friendlyName: 'Table leave',
  description: '',

  inputs: {
    tableId: { type: 'string', reqiured: true }
  },

  exits: {

  },

  fn: async function (inputs) {
    if (this.req.session.user == null) {
      sails.log.error('user not logged in');
      return exits.notLoggedIn();
    }

    await sails.getDatastore().transaction(async (db) => {
      let table = await Tablex.findOne({id: inputs.tableId}).populate('players').usingConnection(db);

      if (table.state != 'WAITING_FOR_PLAYERS') {
        sails.log.error('user can only leave before game has begun');
        this.res.status(400).json({success: false});
      }

      if (this.req.session.user.id == table.owner) {
        await Tablex.update({id: inputs.tableId}).set({state: 'CLOSED'}).usingConnection(db);
        for (let i=0; i < table.players.length; i++) {
          await Tablex.removeFromCollection(inputs.tableId, 'players').members([table.players[i].id]).usingConnection(db);
          await User.removeFromCollection(table.players[i].user, 'playerSession').members([table.players[i].id]).usingConnection(db);
          await Player.destroy(table.players[i].id).usingConnection(db);
        }
      } else {
        let leavingPlayer = table.players.find((p) => {
          return p.user == this.req.session.user.id;
        });
        sails.log.debug(leavingPlayer);

        await Tablex.removeFromCollection(inputs.tableId, 'players').members([leavingPlayer.id]).usingConnection(db);
        await User.removeFromCollection(this.req.session.user.id, 'playerSession').members([leavingPlayer.id]).usingConnection(db);
        await Player.destroy(leavingPlayer.id).usingConnection(db);
      }
    });

    delete this.req.session.chair[inputs.tableId];

    this.res.redirect('/');
  }
};
