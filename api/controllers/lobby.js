module.exports = {
  friendlyName: 'Lobby',
  description: 'Lobby something.',

  inputs: {
    username: { type: 'string', required: false },
    password: { type: 'string', required: false },
    action: { type: 'string', required: false },
  },

  exits: {
    success: {
      responseType: 'view',
      viewTemplatePath: 'pages/lobby'
    }
  },

  fn: async function (inputs, exits) {
    let errMsg = null;

    if (this.req.session.user != null) {
      // just skip the other logic
    } else if (inputs.username != null) {
      if (inputs.password == null || inputs.password.length == 0) {
        errMsg = 'Missing password';
      } else if (inputs.action == 'Register') {
        let nameCollisionCheck = await User.findOne({ username: inputs.username });
        if (nameCollisionCheck) {
          errMsg = 'Username already taken';
        } else {
          this.req.session.user = await User.create({
            id: await sails.helpers.idHelper(),
            username: inputs.username,
            password: await sails.helpers.passhash(inputs.password)
          }).fetch();
          delete this.req.session.user['password'];
        }
      } else {
        let attemptedUser = await User.findOne({ username: inputs.username });
        if (attemptedUser != null) {
          if (attemptedUser.password == await sails.helpers.passhash(inputs.password)) {
            this.req.session.user = attemptedUser;
            delete this.req.session.user['password'];
          } else {
            sails.log.info('failed login for user '+inputs.username);
            errMsg = 'Login failed';
          }
        }
      }
    }

    return exits.success({
      errMsg: errMsg,
      user: this.req.session.user
    });
  }
};
