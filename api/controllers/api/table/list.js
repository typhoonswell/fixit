module.exports = {
  friendlyName: 'List',
  description: 'List table.',

  inputs: {
    page: { type: 'number', required: false, defaultsTo: 1 },
    pagesize: { type: 'number', required: false, defaultsTo: 20 },
    tab: { type: 'string', required: true }
  },

  exits: {

  },

  fn: async function (inputs) {
    let tables = [];
    await sails.getDatastore().transaction(async (db) => {
      switch(inputs.tab) {
        case 'ALL':
          tables = await Tablex.find({
            skip: (inputs.page-1)*inputs.pagesize,
            limit: inputs.pagesize,
            sort: 'updatedAt ASC'
          }).populate('owner').populate('players').usingConnection(db);
          break;
        case 'OPEN':
          tables = await Tablex.find({
            where: {state: ['WAITING_FOR_PLAYERS']},
            skip: (inputs.page-1)*inputs.pagesize,
            limit: inputs.pagesize,
            sort: 'updatedAt ASC'
          }).populate('owner').populate('players').usingConnection(db);
          tables = tables.filter(val => val.players.length < 6);
          break;
        case 'OWNED':
          if (this.req.session.user == null) {
            sails.log.info('user must be logged in to see owned tables');
          } else {
            tables = await Tablex.find({
              where: {owner: this.req.session.user.id},
              skip: (inputs.page-1)*inputs.pagesize,
              limit: inputs.pagesize,
              sort: 'updatedAt ASC'
            }).populate('owner').populate('players').usingConnection(db);
          }
          break;
        case 'SEATED':
          if (this.req.session.user == null) {
            sails.log.info('user must be logged in to see seated tables');
          } else {
            let mySeats = await Player.find({
              where: {user: this.req.session.user.id},
              skip: (inputs.page-1)*inputs.pagesize,
              limit: inputs.pagesize,
              sort: 'updatedAt ASC'
            }).populate('seatedAt').usingConnection(db);
            myTableIds = mySeats.map((val) => {return val.seatedAt.id});
            tables = await Tablex.find({
              where: {
                'and': [
                  { id: myTableIds },
                  { state: ['WAITING_FOR_PLAYERS', 'IN_PROGRESS'] }
                ]
              },
              skip: (inputs.page-1)*inputs.pagesize,
              limit: inputs.pagesize,
              sort: 'updatedAt ASC'
            }).populate('owner').populate('players').usingConnection(db);
          }
          break;
        case 'CLOSED':
          tables = await Tablex.find({
            where: {state: ['CLOSED', 'COMPLETE']},
            skip: (inputs.page-1)*inputs.pagesize,
            limit: inputs.pagesize,
            sort: 'updatedAt ASC'
          }).populate('owner').populate('players').usingConnection(db);
          break;
        default:
          sails.log.info('cannot list tables by ' + inputs.tab);
      }
    });

    for (let i=0; i<tables.length; i++) {
      tables[i].players = tables[i].players.length;
    }

    return this.res.status(200).json(tables);
  }
};
