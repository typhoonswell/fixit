module.exports = {
  friendlyName: 'State',
  description: 'State chat.',

  inputs: {
    tableId: { type: 'string', required: true },
    timestamp: { type: 'number', required: true }
  },

  exits: {
  },

  fn: async function (inputs) {
    if (this.req.session.user == null) {
      sails.log.info('user not authorized to query this chat');
      this.res.status(400).json({success: false});
      return;
    }

    if (this.req.session.chair == null || ! (inputs.tableId in this.req.session.chair)) {
      sails.log.info('user cannot submit chat to table they are not seated at');
      this.res.status(400).json({success: false});
      return;
    }

    let data = [];
    let entries = [];

    await sails.getDatastore().transaction(async (db) => {
      entries = await ChatEntry.find(
        {
          'and': [
            { table: inputs.tableId, },
            { createdAt: { '>': inputs.timestamp } }
          ]
        }).sort('createdAt ASC').usingConnection(db);
    });

    for(let i=0; i < entries.length; i++) {
      data.push({
        timestamp: entries[i].createdAt,
        author: entries[i].author,
        text: entries[i].text
      });
    }

    this.res.status(200).json(data);
  }
};
