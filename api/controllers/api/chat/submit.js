module.exports = {
  friendlyName: 'Submit',
  description: 'Submit chat.',

  inputs: {
    tableId: { type: 'string', required: true },
    author: { type: 'string', required: true },
    text: { type: 'string', required: true },
  },

  exits: {
  },

  fn: async function (inputs) {
    if (this.req.session.user == null || this.req.session.user.username != inputs.author) {
      sails.log.info('user not authorized to submit to this chat');
      this.res.status(400).json({success: false});
      return;
    }

    if (this.req.session.chair == null || ! (inputs.tableId in this.req.session.chair)) {
      sails.log.info('user cannot submit chat to table they are not seated at');
      this.res.status(400).json({success: false});
      return;
    }

    if (inputs.text == null || inputs.text.length == 0) {
      sails.log.info('invalid chat message');
      this.res.status(400).json({success: false});
      return;
    }

    inputs.text = inputs.text.replace(/tony(?= sucks| sux)/gi, 'nico');

    await sails.getDatastore().transaction(async (db) => {
      await ChatEntry.create({
        id: await sails.helpers.idHelper(),
        table: inputs.tableId,
        author: inputs.author,
        text: inputs.text
      })
    });

    this.res.status(200).json({success: true});
  }
};
