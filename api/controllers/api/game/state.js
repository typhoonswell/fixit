module.exports = {
  friendlyName: 'State',
  description: 'State game.',

  inputs: {
    gameId: { type: 'string', required: true },
    turnNumber: { type: 'number', required: true }
  },

  exits: {

  },

  fn: async function (inputs) {
    if (this.req.session.user == null) {
      sails.log.info('cannot query a game without a user');
      this.res.status(400).json({success: false});
      return;
    }

    let response = {};
    let game = null;
    let table = null;
    let turns = null;
    let players = null;

    await sails.getDatastore().transaction(async (db) => {
      game = await Game.findOne({id: inputs.gameId}).usingConnection(db);
      table = await Tablex.findOne({id: game.active}).usingConnection(db);
      turns = await Turn.find(
        {
          'and': [
            { game: inputs.gameId },
            { number: { '>=': inputs.turnNumber } }
          ]
        })
      .sort('number ASC')
      .populate('cardSelection')
      .usingConnection(db);
      players = await Player.find({seatedAt: table.id}).sort('createdAt ASC').usingConnection(db);
    });

    // reformat data out of the table
    for(let i=0; i<turns.length; i++) {
      turns[i].playerStates = JSON.parse(turns[i].playerStates);
      if (turns[i].cardSelection != null) {
        if (turns[i].cardSelection.tableau != null
            && turns[i].cardSelection.tableau.length > 0)
        {
          turns[i].cardSelection.tableau = JSON.parse(turns[i].cardSelection.tableau);
        }
        if (turns[i].cardSelection.finalGuesses != null
            && turns[i].cardSelection.finalGuesses.length > 0)
        {
          turns[i].cardSelection.finalGuesses = JSON.parse(turns[i].cardSelection.finalGuesses);
        }
      }
      if (turns[i].scoreDeltas != null && turns[i].scoreDeltas.length > 0) {
        turns[i].scoreDeltas = JSON.parse(turns[i].scoreDeltas);
      }
      if (turns[i].finalState != null && turns[i].finalState.length > 0) {
        turns[i].finalState = JSON.parse(turns[i].finalState);
      }
    }
    for(let i=0; i<players.length; i++) {
      if (players[i].hand.length > 0) {
        players[i].hand = JSON.parse(players[i].hand);
      }
    }

    response['player'] = players.find((val) => {return val.user == this.req.session.user.id;});
    if (turns.length > 0) {
      response['playerScores'] = JSON.parse(game.scores);
      response['currentTurn'] = turns.splice(-1,1)[0];
      response['turnHistory'] = turns;
      response['imTheLead'] = response['player'].name == players[response['currentTurn'].leadPlayer].name;
      response['turnsRemaining'] = Math.trunc((JSON.parse(game.deck).length)/players.length);
      if (response['currentTurn'].phase >= 2) {
        delete response['currentTurn'].cardSelection.leaderCardNumber;
        let choices = response['currentTurn'].cardSelection.tableau.filter(val => val.cardNumber != null);
        let selectedMap = {};
        for (let i=0; i < choices.length; i++) {
          selectedMap[choices[i].name] =
            (choices[i].name == response['player'].name ? choices[i].cardNumber : null);
        }

        response['tableauPlayerChoices'] = selectedMap;
        if (response['currentTurn'].phase == 3) {
          response['tableau'] = response['currentTurn'].cardSelection.tableau.map(val => val.cardNumber);
          response['finalGuessesSubmitted'] = players.filter(val => val.turnGuess != 0).map(val => val.name);
        }
      }
    } else {
      response['playersSeated'] = players.map(val => val.name);
      response['currentTurn'] = {number: 0, phase: 0};
    }
    response['table'] = table;

    this.res.status(200).json(response);
  }
};
