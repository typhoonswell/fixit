module.exports = {
  friendlyName: 'Submit card selection',
  description: '',

  inputs: {
    tableId: { type: 'string', required: true },
    gameId: { type: 'string', required: true },
    cardIndex: { type: 'number', required: true }
  },

  exits: {
    errorLead: {
      description: ''
    },
    errorPhase: {
      description: ''
    }
  },

  fn: async function (inputs) {
    if (this.req.session.user == null) {
      sails.log.info('user not logged in');
      this.res.status(400).json({success: false});
      return;
    }

    // 0-index the cardIndex
    inputs.cardIndex--;
    let error = false;
    await sails.getDatastore().transaction(async (db) => {
      let turns = await Turn.find({game: inputs.gameId}).sort('number DESC').limit(1).populate('cardSelection').usingConnection(db);
      let turn = turns[0];
      let player = await Player.findOne({
        and: [
          {user: this.req.session.user.id},
          {seatedAt: inputs.tableId}
        ]
      }).usingConnection(db);

      turn.playerStates = JSON.parse(turn.playerStates);

      if (turn.playerStates[turn.leadPlayer].name == player.name) {
        sails.log.info('not your turn to submit');
        error = true;
        throw 'errorLead';
      }

      if (turn.phase != 2) {
        sails.log.error('wrong phase to submit card');
        error = true;
        throw 'errorPhase';
      }

      // update my player state to waiting
      let myPlayerState = turn.playerStates.find(val => val.name == player.name);
      myPlayerState.state = 'WAITING';

      // add the selected card and clue to the tableau
      player.hand = JSON.parse(player.hand);
      let mTableau = JSON.parse(turn.cardSelection.tableau);
      mTableau.find((val) => {return val.name == player.name})['cardNumber'] = player.hand[inputs.cardIndex];

      // randomize tableau
      for (let i = mTableau.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [mTableau[i], mTableau[j]] = [mTableau[j], mTableau[i]];
      }
      let mFinalGuesses = {};
      for (let i=0; i < mTableau.length; i++) {
        mFinalGuesses[mTableau[i].cardNumber] = [];
      }

      await CardSelection.update({id: turn.cardSelection.id}).set({
        tableau: JSON.stringify(mTableau),
        finalGuesses: JSON.stringify(mFinalGuesses)
      }).usingConnection(db);

      if(turn.playerStates.filter(val => val.state == 'WAITING').length == turn.playerStates.length) {
        sails.log.debug('WE HAVE A QUORUM');
        // remove cards from hand
        let allPlayers = await Player.find({seatedAt: inputs.tableId}).sort('createdAt ASC').usingConnection(db);
        for (let i=0; i < allPlayers.length; i++) {
          if (i != turn.leadPlayer) {
            // inefficient I know
            let cardNumberISelected = mTableau.find(val => val.name == allPlayers[i].name).cardNumber;
            allPlayers[i].hand = JSON.parse(allPlayers[i].hand);
            allPlayers[i].hand = allPlayers[i].hand.map(val => (val == cardNumberISelected ? null : val));
            await Player.update({id: allPlayers[i].id}).set({
              hand: JSON.stringify(allPlayers[i].hand)
            }).usingConnection(db);
          }
        }
        //reset the player states
        for (let i=0; i < turn.playerStates.length; i++) {
          if (i == turn.leadPlayer) {
            turn.playerStates[i].state = 'WAITING';
          } else {
            turn.playerStates[i].state = 'GUESSING_LEAD_CARD';
          }
        }
        //change the turn
        turn.phase = 3;
      }

      await Turn.updateOne({id: turn.id}).set({
        phase: turn.phase,
        playerStates: JSON.stringify(turn.playerStates)
      }).usingConnection(db);
    });

    if (!error) {
      this.res.status(200).json({success: true});
    } else {
      this.res.status(400).json({success: false});
    }
  }
};
