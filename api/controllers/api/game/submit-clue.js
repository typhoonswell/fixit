module.exports = {
  friendlyName: 'Submit clue',
  description: '',

  inputs: {
    tableId: { type: 'string', required: true },
    gameId: { type: 'string', required: true },
    cardIndex: { type: 'number', required: true },
    clue: { type: 'string', required: true }
  },

  exits: {
    errorNotLead: {
      description: ''
    },
    errorPhase: {
      description: ''
    }
  },

  fn: async function (inputs) {
    if (this.req.session.user == null) {
      sails.log.info('user not logged in');
      this.res.status(400).json({success: false});
      return;
    }

    if (inputs.clue.length == 0) {
      sails.log.info('submitted bad clue');
      this.res.status(400).json({success: false});
      return;
    }

    // 0-index the cardIndex
    inputs.cardIndex--;

    let error = false;
    await sails.getDatastore().transaction(async (db) => {
      let turns = await Turn.find({game: inputs.gameId}).sort('number DESC').limit(1).usingConnection(db);
      let turn = turns[0];
      let player = await Player.findOne({
        and: [
          {user: this.req.session.user.id},
          {seatedAt: inputs.tableId}
        ]
      }).usingConnection(db);

      turn.playerStates = JSON.parse(turn.playerStates);

      if (turn.playerStates[turn.leadPlayer].name != player.name) {
        sails.log.info('not your turn to submit');
        error = true;
        throw 'errorNotLead';
      }

      if (turn.phase != 1) {
        sails.log.error('wrong phase to submit clue');
        error = true;
        throw 'errorPhase';
      }

      //add that card to the board with association to submitter
      player.hand = JSON.parse(player.hand);
      let newTableau = [{
        name: player.name,
        lead: true,
        cardNumber: player.hand[inputs.cardIndex]
      }];
      for(let i=0; i<turn.playerStates.length; i++) {
        if (turn.playerStates[i].name != player.name) {
          newTableau.push({
            name: turn.playerStates[i].name,
            lead: false,
            cardNumber: null
          });
          turn.playerStates[i].state = 'SELECTING_CARD';
        } else {
          turn.playerStates[i].state = 'WAITING';
        }
      }

      // add the selected card and clue to the tableau
      let mCardSelection = await CardSelection.create({
        id: await sails.helpers.idHelper(),
        leaderCardNumber: player.hand[inputs.cardIndex],
        tableau: JSON.stringify(newTableau)
      }).usingConnection(db).fetch();

      // remove card from submitter's hand
      player.hand[inputs.cardIndex] = null;
      await Player.updateOne({id: player.id}).set({hand: JSON.stringify(player.hand)}).usingConnection(db);

      // update the turn
      await Turn.update({id: turn.id}).set({
        phase: 2,
        clue: inputs.clue,
        cardSelection: mCardSelection.id,
        playerStates: JSON.stringify(turn.playerStates)
      }).usingConnection(db);
    });

    if (!error) {
      this.res.status(200).json({success: true});
    } else {
      this.res.status(400).json({success: false});
    }
  }
};
