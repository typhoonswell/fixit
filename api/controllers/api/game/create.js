module.exports = {
  friendlyName: 'Create game',
  description: '',

  inputs: {
    name: { type: 'string', required: false },
    password: { type: 'string', required: false }
  },

  exits: {
    success: {
      description: 'Game created successfully.'
    },
    notLoggedIn: {
      description: 'Must be logged in to create games.'
    }
  },

  fn: async function (inputs, exits) {

    if (this.req.session.user == null) {
      sails.log.error('user not logged in');
      return exits.notLoggedIn();
    }

    await sails.getDatastore().transaction(async (db) => {
      let newgame = await Game.create({
        id: await sails.helpers.idHelper()
      }).usingConnection(db).fetch();

      let newtable = await Tablex.create({
        id: await sails.helpers.idHelper(),
        name: inputs.name,
        password: inputs.password || null,
        state: 'WAITING_FOR_PLAYERS',
        owner: this.req.session.user.id,
        servingGame: newgame.id
      }).usingConnection(db).fetch();

      let player = await Player.create({
        id: await sails.helpers.idHelper(),
        name: this.req.session.user.username,
        seatedAt: newtable.id,
        user: this.req.session.user.id,
        turnGuess: 0
      }).usingConnection(db).fetch();

      await Tablex.addToCollection(newtable.id, 'players', player.id).usingConnection(db);

      await Game.updateOne({id: newgame.id}).set({active: newtable.id}).usingConnection(db);
    });

    this.res.status(200).json({success: true});
  }
};
