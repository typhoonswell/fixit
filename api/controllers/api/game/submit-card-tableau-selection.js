module.exports = {
  friendlyName: 'Submit card tableau selection',
  description: '',

  inputs: {
    tableId: { type: 'string', required: true },
    gameId: { type: 'string', required: true },
    cardIndex: { type: 'number', required: true }
  },

  exits: {
    errorLead: {
      description: ''
    },
    errorPhase: {
      description: ''
    },
    errorSelection: {
      description: ''
    }
  },

  fn: async function (inputs) {
    if (this.req.session.user == null) {
      sails.log.info('user not logged in');
      this.res.status(400).json({success: false});
      return;
    }

    // 0-index the cardIndex
    inputs.cardIndex--;
    let error = false;
    await sails.getDatastore().transaction(async (db) => {
      let game = await Game.findOne({id: inputs.gameId}).usingConnection(db);
      let turns = await Turn.find({game: inputs.gameId}).sort('number DESC').limit(1).populate('cardSelection').usingConnection(db);
      let turn = turns[0];
      let player = await Player.findOne({
        and: [
          {user: this.req.session.user.id},
          {seatedAt: inputs.tableId}
        ]
      }).usingConnection(db);

      turn.playerStates = JSON.parse(turn.playerStates);

      if (turn.playerStates[turn.leadPlayer].name == player.name) {
        sails.log.info('not your turn to submit');
        error = true;
        throw 'errorLead';
      }

      if (turn.phase != 3) {
        sails.log.error('wrong phase to submit card');
        error = true;
        throw 'errorPhase';
      }

      // update my player state to waiting
      let myPlayerState = turn.playerStates.find(val => val.name == player.name);
      myPlayerState.state = 'WAITING';

      // add card to final guess
      let mFinalGuesses = JSON.parse(turn.cardSelection.finalGuesses);
      let mTableau = JSON.parse(turn.cardSelection.tableau);
      player.finalGuess = mTableau[inputs.cardIndex].cardNumber;

      // player shouldn't send a guess twice in the same turn
      if (mFinalGuesses[''+player.finalGuess].includes(player.name)) {
        sails.log.info('player already submitted tableau selection');
        error = true;
        throw 'errorSelection';
      }
      mFinalGuesses[''+player.finalGuess].push(player.name);

      await CardSelection.update({id: turn.cardSelection.id}).set({
        finalGuesses: JSON.stringify(mFinalGuesses)
      }).usingConnection(db);

      await Turn.updateOne({id: turn.id}).set({
        playerStates: JSON.stringify(turn.playerStates)
      }).usingConnection(db);

      if (turn.playerStates.filter(val => val.state == 'WAITING').length == turn.playerStates.length) {
        sails.log.debug('WE HAVE A QUORUM');
        let allPlayers = await Player.find({seatedAt: inputs.tableId}).sort('createdAt ASC').usingConnection(db);
        let endOfGame = false;
        // update scores
        let cardOwners = {};
        let mScoreDeltas = {};
        for (let i=0; i < mTableau.length; i++) {
          cardOwners[mTableau[i].cardNumber] = mTableau[i].name;
          mScoreDeltas[mTableau[i].name] = 0;
        }
        let leadPlayerName = allPlayers[turn.leadPlayer].name;
        let leadPlayerCardNumber = mTableau.find(val => val.name == leadPlayerName).cardNumber;
        let everyoneOrNoone = false;
        if (mFinalGuesses[leadPlayerCardNumber].length == 0 || mFinalGuesses[leadPlayerCardNumber].length == (allPlayers.length-1)) {
          everyoneOrNoone = true;
        }
        for (const cNum in mFinalGuesses) {
          if (cardOwners[cNum] != leadPlayerName) {
            mScoreDeltas[cardOwners[cNum]]+=mFinalGuesses[cNum].length;
          } else {
            if (!everyoneOrNoone) {
              mScoreDeltas[leadPlayerName] += 3;
              for (let i=0; i < mFinalGuesses[cNum].length; i++) {
                mScoreDeltas[mFinalGuesses[cNum][i]] += 3;
              }
            } else {
              for (const pName in mScoreDeltas) {
                if (pName != leadPlayerName) {
                  mScoreDeltas[pName] += 2;
                }
              }
            }
          }
        }

        // check for end of game
        let reason = null;
        game.scores = JSON.parse(game.scores);
        game.deck = JSON.parse(game.deck);
        if (game.deck.length < allPlayers.length) {
          endOfGame = true;
          reason = 'DECK_EMPTY';
        }

        for (const pName in mScoreDeltas) {
          game.scores[pName] += mScoreDeltas[pName];
          if (game.scores[pName] >= 30) {
            endOfGame = true;
            reason = 'PLAYER_SCORE';
          }
        }

        // re-deal cards to vacant spot in the hands or end the game if no more cards
        if (!endOfGame) {
          for (let i = 0; i < allPlayers.length; i++) {
            allPlayers[i].hand = JSON.parse(allPlayers[i].hand);
            let emptySpot = allPlayers[i].hand.findIndex(val => val == null);
            allPlayers[i].hand[emptySpot] = game.deck.splice(0,1)[0];
            await Player.updateOne({id: allPlayers[i].id}).set({hand: JSON.stringify(allPlayers[i].hand)}).usingConnection(db);
          }

          //change the turn
          let newLeadPlayer = (turn.leadPlayer == (turn.playerStates.length-1) ? 0 : turn.leadPlayer+1);
          let mPlayerStates = [];
          let mPlayerScores = {};
          for (let i = 0; i < allPlayers.length; i++) {
            mPlayerScores[allPlayers[i].name] = 0;

            if (i == newLeadPlayer) {
              mPlayerStates.push({name: allPlayers[i].name, state: 'SELECTING_CLUE'});
            } else {
              mPlayerStates.push({name: allPlayers[i].name, state: 'WAITING'});
            }
          }
          let newTurn = await Turn.create({
            id: await sails.helpers.idHelper(),
            number: turn.number+1,
            clue: null,
            phase: 1,
            playerStates: JSON.stringify(mPlayerStates),
            leadPlayer: newLeadPlayer,
            game: inputs.gameId
          }).usingConnection(db).fetch();
        } else {
          await Tablex.updateOne({id: inputs.tableId}).set({state: 'COMPLETE'}).usingConnection(db);
        }

        await Turn.updateOne({id: turn.id}).set({
          phase: 4,
          finalState: JSON.stringify(game.scores),
          scoreDeltas: JSON.stringify(mScoreDeltas)
        }).usingConnection(db);

        await Game.updateOne({id: inputs.gameId}).set({
          deck: JSON.stringify(game.deck),
          scores: JSON.stringify(game.scores),
          endOfGameReason: reason
        }).usingConnection(db);
      }
    });

    if (!error) {
      this.res.status(200).json({success: true});
    } else {
      this.res.status(400).json({success: false});
    }
  }
};
