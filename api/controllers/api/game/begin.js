module.exports = {
  friendlyName: 'Begin',
  description: 'Begin game.',

  inputs: {
    gameId: { type: 'string', required: true }
  },

  exits: {
  },

  fn: async function (inputs) {
    if (this.req.session.user == null) {
      sails.log.info('cannot begin a game without a user');
      this.res.status(400).json({success: false});
      return;
    }

    let game = await Game.findOne({id: inputs.gameId}).populate('turns');
    if(game.turns.length > 0) {
      sails.log.info('game has already begun');
      this.res.status(400).json({success: false});
      return;
    }

    await sails.getDatastore().transaction(async (db) => {
      let table = await Tablex.findOne({servingGame: inputs.gameId}).populate('players').usingConnection(db);
      let players = await Player.find({
        id: {
          'in': table.players.map((val) => {
            return val.id
          })
        }
      }).sort('createdAt ASC').usingConnection(db);

      if (this.req.session.user.id != table.owner) {
        sails.log.info('only the table owner can begin the game');
        this.res.status(400).json({success: false});
        return;
      }

      if (players.length < 4 || players.length > 6) {
        sails.log.info('can\'t begin a game with ' + players.length + ' players');
        this.res.status(400).json({success: false});
        return;
      }

      // close the table
      await Tablex.updateOne({id: table.id}).set({state: 'IN_PROGRESS'}).usingConnection(db);

      let mDeck = [];
      for (let k = 0; k < 84; k++) {
        mDeck.push(k + 1);
      }

      // shuffle
      for (let i = mDeck.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [mDeck[i], mDeck[j]] = [mDeck[j], mDeck[i]];
      }

      sails.log.debug(mDeck);

      //deal initial hands and set states
      let mPlayerStates = [];
      let mPlayerScores = {};
      for (let i = 0; i < players.length; i++) {
        let mHand = mDeck.splice(0, 6);
        await Player.updateOne({id: players[i].id}).set({hand: JSON.stringify(mHand)}).usingConnection(db);
        mPlayerScores[players[i].name] = 0;

        if (players[i].user == table.owner) {
          mPlayerStates.push({name: players[i].name, state: 'SELECTING_CLUE'});
        } else {
          mPlayerStates.push({name: players[i].name, state: 'WAITING'});
        }
      }

      await Game.updateOne({id: inputs.gameId}).set({deck: JSON.stringify(mDeck), scores: JSON.stringify(mPlayerScores)}).usingConnection(db);

      let newTurn = await Turn.create({
        id: await sails.helpers.idHelper(),
        number: 1,
        clue: null,
        phase: 1,
        playerStates: JSON.stringify(mPlayerStates),
        game: inputs.gameId
      }).usingConnection(db).fetch();

      await Game.addToCollection(inputs.gameId, 'turns', newTurn.id).usingConnection(db);
    });

    this.res.status(200).json({success: true});
  }
};
