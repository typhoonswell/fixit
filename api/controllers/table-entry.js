let error = false;
module.exports = {
  friendlyName: 'Table entry',
  description: '',

  inputs: {
    tableId: { type: 'string', required: true },
    password: {type: 'string', required: false }
  },

  exits: {
    cantEnter: {
      description: 'User cannot enter table.',
      viewTemplatePath: 'pages/lobby'
    }
  },

  fn: async function (inputs) {
    if (this.req.session.user == null) {
      this.res.redirect('/');
      return;
    }

    let selectedTable = await Tablex.findOne({id: inputs.tableId}).populate('players');
    if (!selectedTable || selectedTable.state == 'CLOSED') {
      sails.log.info('table unavailable');
      this.res.redirect('/');
      return;
    }

    let player = null;
    let error = false;
    await sails.getDatastore().transaction(async (db) => {
      let seatedPlayerIds = selectedTable.players.map((val) => {
        return val.id;
      });

      let seatedPlayers = await Player.find({id: {'in': seatedPlayerIds}}).usingConnection(db);
      let seatedUserIds = seatedPlayers.map((val) => {
        return val.user;
      });
      let alreadySeated = seatedUserIds.includes(this.req.session.user.id);
      if (alreadySeated) {
        sails.log.info('player ' + this.req.session.user.username + ' already seated');
        let playerName = this.req.session.user.username;
        if (selectedTable.password != null) {
          sails.log.info('challenging password');
          if (selectedTable.password != inputs.password) {
            sails.log.info('player ' + playerName + ' failed password');
            throw 'cantEnter';
          }
        }
        player = await Player.findOne({
          and: [
            {user: this.req.session.user.id},
            {seatedAt: inputs.tableId}
          ]
        }).usingConnection(db);
      } else if (selectedTable.state != 'WAITING_FOR_PLAYERS') {
        sails.log.info('player ' + this.req.session.user.username + ' tried to enter a running game');
        throw 'cantEnter';
      } else if (seatedPlayerIds.length >= 6) {
        sails.log.info('player cannot enter because table is full');
        throw 'cantEnter';
      } else {
        let playerName = this.req.session.user.username;
        if (selectedTable.password != null) {
          sails.log.info('challenging password');
          if (selectedTable.password != inputs.password) {
            sails.log.info('player ' + playerName + ' failed password');
            throw 'cantEnter';
          }
        }

        player = await Player.create({
          id: await sails.helpers.idHelper(),
          name: playerName,
          seatedAt: inputs.tableId,
          user: this.req.session.user.id,
          turnGuess: 0
        }).usingConnection(db).fetch();
      }
    }).tolerate(() => {error = true; return error;});

    if (error) {
      this.res.redirect('/');
      return;
    }

    if (this.req.session.chair == null) {
      sails.log.debug('init session chair');
      this.req.session.chair = {};
    }
    this.req.session.chair[inputs.tableId] = player.id;
    sails.log.debug(this.req.session.chair);
    this.res.redirect('/table/'+inputs.tableId);
  }
};
